<?php
require"../core/access.php";

include ("stat_count.php");
?>

<!DOCTYPE HTML>
<html>
<head>

  <link rel="stylesheet" type="text/css" href="template/style.css">

  <script type="text/javascript" src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
</head>
<body>
  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
  <div class='row text-center'>
    <a href="statcsv.php?abs=<?php echo 'absences' ; ?>">Cliquez ici pour exporter le fichier</a>
    <a href='admin.php' class="btn btn-primary">Retour</a></br>
  </div>
  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Statisc by week, month and promotion"
      },
      data: [

        {
          dataPoints: [
            { y: <?php echo $result_week[0] ; ?>, label: "Semaine"},
            { y: <?php echo $result_month[0] ; ?>,  label: "Mois" },
            { y: <?php echo $result_twomonths[0]; ?>, label: "2 Mois" },
            { y: <?php echo $result_promo[0] ; ?>,  label: "6 mois "},
            { y: <?php echo $result_abs[0] ; ?>, label: "Absence total"},
            { y: <?php echo $result_abs_null[0]; ?>, label: "Absences non justifées"}

          ]
        }
      ]
    });

    chart.render();
  }
  </script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
