<?php
require"../core/access.php";
session_start();
include("header.php");
include("topbar.php");
include("../core/promo.php");

?>

<div class="row">
  <form method='post' action='user.php'>
    <input type="hidden" name="idpromo" value="<?=$idpromo?>">
    <label  class='col-md-4 col-md-offset-1' for="promo">Sélectionner une promotion :</label>

    <select  class='col-md-2 form-group ' name='idpromo' class="form-control">
      <?php promo(); ?>
    </select>
    <input class='col-md-1 col-md-offset-3 btn btn-success text-uppercase' type='submit' value='go'>
  </form>
</div>

<div class="container-fluid">
  <div class="row">
    <form method='post' action='../core/add_abs.php'>
      <table class="table">
        <tr>
          <th class='text-center text-uppercase col-md-2'>nom</th>
          <th class='text-center text-uppercase col-md-2'>prénom</th>
          <th class='text-center text-uppercase col-md-4'>etait absent(e) le matin</th>
          <th class='text-center text-uppercase col-md-4'>etait absent(e) l'après-midi</th>
        </tr>
        <?php
          include("../core/students_list.php");
        ?>
      </table>
      <input type='submit' class='btn btn-success col-md-4 col-md-offset-4' value='Enregistrer les absents'>
    </form>
  </div>
  <hr>
  <div class="row">
    <a href='justify.php' title="">
      <button class='btn btn-warning text-uppercase text-center col-md-4 col-md-offset-4'>
        justificatifs
      </button>
    </a>
  </div>



<?php include("footer.php"); ?>
