<?php
function update_rss() {


 $xml = "<?xml version='1.0' encoding='utf-8'?>";
 $xml .= "<feed xmlns='http://www.w3.org/2005/Atom'>";
 $xml .= " <title>Absences des élèves de POP</title>";
 $xml .= " <link></link>";
 $xml .= "<subtitle>le flux des absences des élèves de Popschool</subtitle>";
 $xml .= " <language>fr</language>";
 $xml .= " <category>Absences</category>";
 $xml .= " <generator>PHP/MySQL</generator>";

 include "../config/config.php";
 $query =  "SELECT * FROM abs ORDER BY date DESC LIMIT 0, 30";
 $result= mysqli_query($handle,$query);

 while ($line=mysqli_fetch_array($result)) {
   $xml .= "<entry>";
   $xml .= "<title>".stripcslashes($line['justify'])."</title>";
   // Ne pas oublier de changer le lien !!!
   // $xml .= "<link>.php?id=".$line['']."</link>";
   $xml .= "<updated>".(date('D, d M Y H:i:s O', strtotime($line['date'])))."</updated>";
   $xml .= "</entry>";
 }
 $xml .= "</feed>";
 $rss = fopen("flux_rss.xml", "w+");
 fwrite($rss, $xml);
 fclose($rss);

}

?>