<?php
require"../core/access.php";

include("header.php");
session_start();
include("../core/abs_list.php");
include("topbar.php");
?>

<div class="container-fluid">
  <div class="row">
    <table class="table">
      <tr>
        <th class='text-center text-uppercase'>nom</th>
        <th class='text-center text-uppercase'>prénom</th>
        <th class='text-center text-uppercase'>date</th>
        <th class='text-center text-uppercase'>durée</th>
        <th class='text-center text-uppercase'>actions</th>
      </tr>
      <?php
      delabs();
      absences();
      ?>
    </table>
  </div>

  <a href='admin.php' class="btn btn-primary col-md-2 col-md-offset-5">Retour</a></br>

<?php include("footer.php"); ?>
