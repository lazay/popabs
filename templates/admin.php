<?php
require"../core/access.php";

include("header.php");
session_start();
include("topbar.php");
?>

<div class="row">
  <a href='./users.php' title="">
    <button class='admin-menu btn btn-default text-uppercase text-center col-xs-12 col-md-4 col-md-offset-1'>
        <div class="admin-ui">
            <strong>utilisateurs</strong>
        </div>
    </button>
  </a>
  <a href='stud.php' title="">
    <button class='admin-menu btn btn-default text-uppercase text-center col-xs-12 col-md-4 col-md-offset-2'>
        <div class="admin-ui">
            <strong>eleves</strong>
        </div>
    </button>
  </a>
</div>

<div class="row">
  <a href='calendar.php' title="">
    <button class='admin-menu btn btn-default text-uppercase text-center col-xs-12 col-md-4 col-md-offset-1'>
        <div class="admin-ui">
            <strong>statistiques</strong>
        </div>
    </button>
  </a>
  <a href='justify.php' title="">
    <button class='admin-menu btn btn-default text-uppercase text-center col-xs-12 col-md-4 col-md-offset-2'>
        <div class="admin-ui">
            <strong>justificatifs</strong>
        </div>
    </button>
  </a>
</div>

<div class="row">
  <a href='abs.php' title="">
    <button class='admin-menu btn btn-default text-uppercase text-center col-xs-12 col-md-4 col-md-offset-4'>
        <div class="admin-ui">
            <strong>absences</strong>
        </div>
    </button>
  </a>
</div>



<?php include("footer.php"); ?>
