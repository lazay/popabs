<?php
if  (session_status() == PHP_SESSION_ACTIVE) {
  if(isset($_POST["idpromo"])) {
    $_SESSION["idpromo"] = $_POST["idpromo"];
  }
  else {
    $_SESSION["idpromo"] = 1;
  }
}
?>
<div class="row">
<nav class="navbar nav-color navbar-static-top">
  <div class="container-fluid home-nav">
      <ul class='nav navbar-nav col-md-12 col-sm-12 '>
        <li class="text-center">
          <a href='' title='' class="">
              <button class='btn text-uppercase text-center user-title'>
                Bienvenue <?php echo  $_SESSION['username'];?>
              </button>
          </a>
        </li>
        <li class="navbar-right text-center">
          <a href="../core/logout.php">
            <button class='btn btn-default'>
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </button>
          </a>
        </li>
      </ul>
    </div>
</nav>
</div>
