<?php
require"../core/access.php";

include("header.php");
session_start();
include("topbar.php");
?>


<body>

<form method="post" action="stat.php">
<p class="col-md-4">Date de début: <input type="text" name="startdate" id="startdate"></p>
<p class="col-md-4">Date de fin: <input type="text" name="finishdate" id="finishdate"></p>
<input class="col-md-2 col-md-offset-1" type="submit" >
</form>

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#startdate" ).datepicker(
      ($.datepicker));
  } );
  $( function() {
    $("#finishdate").datepicker(
      ($.datepicker));
  } );
  </script>

  <a href='admin.php' class="btn btn-primary col-md-2 col-md-offset-5">Retour</a></br>

</body>
</html>
