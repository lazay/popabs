<?php
require"../core/access.php";

include("header.php");
session_start();
include("topbar.php");
?>

<div class="container">
    <div class="row">
      <h2 class='text-center'>Enregister une absence programmée :</h2><br>
    </div>
    <div class="col-md-12">
      <form  action="../core/add_scheduled.php" method="post" enctype="multipart/form-data">
        <label for='date'>Entrer la date au format : yyyy-mm-dd </label>
        <input name='date' type='text'>

        <select name='periode'>
          <option value='1'>matin</option>
          <option value='2'>après-midi</option>
          <option value='0'>journée</option>
        </select>
        <div class="row">
          <h2 class='text-center'>Ajouter un justificatif :</h2><br>
          <p class='text-center bg-success'><strong>Seuls les formats pdf, png, jpeg, jpg sont supportés.</strong></p><br>
          <p class='text-center bg-success'><strong>Veuillez renommer votre fichier nom_prénom_yyyymmdd, merci.</strong></p><br>
        </div>
        <input type="file" name="userfile">
        <input class='btn btn-success col-md-2 col-md-offset-5' type="submit">
      </form>
    </div>
</div>

<?php include("footer.php"); ?>
