<?php
require"../core/access.php";

include("header.php");
session_start();
include("topbar.php");
include("../core/justify_list.php");
?>
<div class="container-fluid">
  <div class="row">
    <div class="panel-success">
      <div class="panel-heading text-center text-uppercase">
        absences justifiées
      </div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <th class='text-center text-uppercase'>nom</th>
            <th class='text-center text-uppercase'>prénom</th>
            <th class='text-center text-uppercase'>date</th>
            <th class='text-center text-uppercase'>justificatif</th>
          </tr>
          <?php
            all();
          ?>
        </table>
      </div>
    </div>
    <div class="panel-danger">
      <div class="panel-heading text-center text-uppercase">
        absences injustifiées
      </div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <th class='text-center text-uppercase'>nom</th>
            <th class='text-center text-uppercase'>prénom</th>
            <th class='text-center text-uppercase'>date</th>
            <th class='text-center text-uppercase'>justificatif</th>
          </tr>
          <?php
            withoutjustification();
          ?>
        </table>
      </div>
    </div>
  </div>

  <div class="row">
    <form action='../core/menu.php' title="">
      <br><br><button class='btn btn-primary text-uppercase text-center col-xs-12 col-md-2 col-md-offset-5'>
          home
      </button>
    </a>
  </div>
</div>


<?php include("footer.php"); ?>
