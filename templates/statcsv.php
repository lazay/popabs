<?php
include('../config/config.php');
session_start(); // ici on continue la session

if ((!isset($_SESSION['username'])) || ($_SESSION['username'] == ''))

{

    echo "<h3 class='text-center text-danger text-uppercase bg-danger'>vous devez être connecté</h3>";
    echo "<a href='../index.php' class='btn btn-default col-md-4 col-md-offset-4'>connexion</a>";

    exit();

}

// Web export CSV
header('Content-Type: text/csv');
header('Content-Disposition: attachment;filename='.$_GET['abs'].'.csv');

// Query
$numb_absence="SELECT s.name, s.firstname, a.* FROM abs a INNER JOIN students s ON s.id=a.idstudent";
//$numb_absence="SELECT * FROM abs";
$absence=mysqli_query($handle,$numb_absence);

if ($result_absence) {
	makecsv(array_keys($result_absence));
}
while ($result_absence=mysqli_fetch_assoc($absence)) {
	makecsv($result_absence);
}

// Function
 function makecsv($num_field_names) {
 	$separate = ' ';

 	// replace separator by dashes
 	foreach ($num_field_names as $field_name) {
 		$field_name = str_replace(array('<br>', '</br>', ',' , ';'), array( '-', '-', '-', '-'), $field_name);
 		echo $separate . $field_name;

 		// add excel separator
 		$separate=';';
 	}

 	echo "\r ";
 }


?>
