<?php
require"../core/access.php";

include("header.php");
include("../core/users_list.php");
include("topbar.php");
?>
<div class="container-fluid">
  <div class="row">
    <table class="table">
      <tr>
        <th class='text-center text-uppercase'>nom</th>
        <th class='text-center text-uppercase'>prénom</th>
        <th class='text-center text-uppercase'>status</th>
        <th class='text-center text-uppercase'>supprimer</th>
      </tr>
      <?php
      del();
      users($current);
      ?>
    </table>
  </div>

  <div class="container-fluid">
    <h4 class="text-center text-uppercase">Ajouter un nouvel utilisateur :</h4>
    <form method='post' action='../core/add_user.php'>
      <div class="col-md-5 col-md-offset-1">
        <label for="firstname">Prénom :</label>
        <input class="form-control"id='focusedinput' type=text name='firstname'>
        <label for="name">Nom :</label>
        <input class="form-control" type=text name='name'>
        <label for="status">Status de l'utilisateur :</label>
        <select name='status' class="form-control">
          <option value=2 selected="selected">Administrateur</option>
          <option value=1>Formateur</option>
          <option value=0>Elèves</option>
        </select>
      </div>
      <div class="col-md-5">
        <label for="password">Mot de passe :</label>
        <input class="form-control" type=password name='password'>
        <label for="cfpassword">Confimer le mot de passe :</label>
        <input class="form-control" type=password name='cfpassword'>
        <label for="register"></label>
        <input name='register' class="btn btn-primary btn-block" type=submit value='Enregistrer'>
      </div>
    </form>
  </div>

  <div class="row">
    <a href='admin.php' title="">
      <br><br><button class='btn btn-primary text-uppercase text-center col-xs-12 col-md-2 col-md-offset-5'>
          home
      </button>
    </a>
  </div>
</div>


<?php include("footer.php"); ?>
