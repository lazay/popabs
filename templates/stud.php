<?php
require"../core/access.php";

include("header.php");
include("../config/config.php");
session_start();
include("../core/promo.php");
include("topbar.php");
?>
<div class="row">
	<form action="addstudent.php" method="post">
		<label class='col-md-2' for="name">Ajouter le nom d'un nouvel élève</label>
		<input class='col-md-2' type="text" name="name">
		<label class='col-md-2' for="firstname">Ajouter le prénom d'un élève</label>
		<input class='col-md-2' type="text" name="firstname">
		<select  class='col-md-2' name='idpromo' class="form-control">
			<?php promo(); ?>
		</select>
		<input class='btn btn-default' type="submit">
	</form>
</div>

<?php

$query="SELECT * FROM students INNER JOIN promos WHERE students.idpromo=promos.id";
$result=mysqli_query($handle,$query);

while($line=mysqli_fetch_array($result)) {
	echo "\t<li>". $line["promo"] . " " . $line["name"] . " " . $line["firstname"];
	echo "&nbsp;<a href=\"delete_stud.php?id=". $line["id"] . "\">Supprimer cet élève</a>";
	echo "&nbsp;<a href=\"update_stud.php?id=". $line["id"] . "\">Modifier</a>";
	echo "\t</li>";
}

?>
<form action="../core/addpromo.php" method="post">
	<label for="promo">Ajouter une nouvelle promotion</label>
	<input type="text" name="promo">
	<input type="submit">
</form>

<div class="row">
	<a href='admin.php' title="">
		<br><br><button class='btn btn-primary text-uppercase text-center col-xs-12 col-md-2 col-md-offset-5'>
				home
		</button>
	</a>
</div>

<?php include("footer.php"); ?>
