<?php

session_start();

// Delete variables for session and sessions' variables
session_destroy();

// Delete cookies for automatic connection
setcookie('username', '');
setcookie('pass_hache', '');

header("location: ../index.php");

?>
