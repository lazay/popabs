<?php
include("../templates/header.php");
include("../config/config.php");

$name = $_POST["name"];
$firstname = $_POST["firstname"];
$status = $_POST["status"];
$pass = $_POST["password"];
$cfpass = $_POST["cfpassword"];

$error_empty = "<h3 class='text-center text-danger text-uppercase bg-danger'>vous n'avez pas rempli tout les champs</h3>";
$error_pass = "<h3 class='text-center text-danger text-uppercase bg-danger'>Une erreur est survenue lors de la confirmation du mot de passe</h3>";

// Check password confirmation
if (empty($name)||empty($firstname)||empty($pass)||empty($cfpass)) {
  echo $error_empty;
  echo "<a href=../templates/users.php class='btn btn-default text-uppercase col-md-4 col-md-offset-4'>réessayer</a>";
}
elseif ($pass!=$cfpass) {
  echo $error_pass;
  echo "<a href=../templates/users.php class='btn btn-default text-uppercase col-md-4 col-md-offset-4'>réessayer</a>";
}
else {
  // Hach password
  $pass_hache = sha1($pass);
  // Insertion
  $adduser = "INSERT INTO users(name, firstname, status, password)
  VALUES('$name', '$firstname', '$status', '$pass_hache')";
  $add = mysqli_query($handle,$adduser);

  header('location:../templates/users.php');
}

include("../templates/footer.php");

?>
