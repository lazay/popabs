<?php
include("header.php");
include("../config/config.php");
session_start();
include("topbar.php");

?>

<form action="update_abs.php?id=<?php echo $_GET["id"]?>" method="post">
	<label for="date">Modifier la date :</label>
	<input type="text" name="date">
	<label for="firstname">Modifier la durée (format yyyy-mm-dd):</label>
  <select name='periode'>
    <option value='1'>matin</option>
    <option value='2'>après-midi</option>
    <option value='0'>journée</option>
  </select>
	<input class='btn btn-primary' type="submit" name="update" value='modifier'>
</form>

<?php


if(isset($_POST['update'])) {
	$date=$_POST["date"];
	$period=$_POST["periode"];
	$id=$_GET['id'];

  if($period==1){ $morning=1; $afternoon=0; }
  elseif($period==2){ $morning=0; $afternoon=1; }
  else { $morning=1; $afternoon=1; }

	$query="SELECT * FROM abs WHERE id=".$_GET["id"];
	$result=mysqli_query($handle,$query);

	if($handle->affected_rows > 0) {
		$query=" UPDATE `abs` SET `absdate`='$date', `morning`='$morning', `afternoon`='$afternoon' WHERE `id`='$id'";
		$result = mysqli_query($handle,$query);
		echo "<h3 class='text-center text-success text-uppercase bg-success'>L'absence a bien été modifiée.<br>\n";
	}
	else {
		echo "<h3 class='text-center text-danger text-uppercase bg-danger'>Une erreur est
    survenue lors de la modification de l'absence.</h3><br>\n";
	}
}

?>

<a class='btn btn-default col-md-4 col-md-offset-4' href="../templates/abs.php">retour</a>


<?php include("footer.php"); ?>
