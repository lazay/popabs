<?php
include("header.php");
session_start(); // ici on continue la session

if ((!isset($_SESSION['username'])) || ($_SESSION['username'] == ''))

{

    echo "<h3 class='text-center text-danger text-uppercase bg-danger'>vous devez être connecté</h3>";
    echo "<a href='../index.php' class='btn btn-default col-md-4 col-md-offset-4'>connexion</a>";

    exit();

}
include("footer.php");
?>
