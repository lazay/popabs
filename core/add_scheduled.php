<?php
include("../templates/header.php");
include("../config/config.php");
session_start();

$date=$_POST['date'];
$current = $_SESSION['username'];

//Get current user id
$query="SELECT id FROM students WHERE firstname LIKE '$current'";
$result=mysqli_fetch_array(mysqli_query($handle,$query));
$idstudent=$result['id'];

$abs=$_POST['periode'];

if($abs==1){ $morning=1; $afternoon=0; }
elseif($abs==2){ $morning=0; $afternoon=1; }
else { $morning=1; $afternoon=1; }

if(empty($date)){
  echo "<h3 class='text-center text-uppercase bg-danger text-danger'>veuillez indiquer la date de votre absence</h3>";
  echo "<a href='../templates/student.php' class='btn btn-default text-uppercase col-md-4 col-md-offset-4'>recommencer</a>";
}
else{
$info = pathinfo($_FILES['userfile']['name']);
$extension_info= $info['extension'];
$extensions= array(
    'pdf',
    'jpg',
    'jpeg',
		'png'
);

  if (in_array($extension_info, $extensions)) {
      $name      = $_FILES["userfile"]["name"];
      $tmp_name  = $_FILES['userfile']['tmp_name'];
      $error     = $_FILES['userfile']['error'];
      $location1 = '/var/www/html/popabs/uploads/';
      if (move_uploaded_file($tmp_name, $location1 . $name)) {
          echo "<h3 class='text-center text-uppercase bg-success text-success'>Merci</h3>";
          echo "<br>";
      }
      if ($handle->affected_rows > 0) {
          echo "<h3 class='text-center text-uppercase bg-success text-success'>Votre absence et son justificatif sont enregistrés</h3><br>";
          echo "<a href='../templates/student.php' class='btn btn-default text-uppercase col-md-4 col-md-offset-4'>retour</a>";
      } else {
          echo "<h3 class='text-center text-uppercase bg-danger text-danger'>Erreur pendant l'upload du justificatif</h3>";
          echo "<a href='../templates/student.php' class='btn btn-default text-uppercase col-md-4 col-md-offset-4'>recommencer</a>";
      }
  }
  else {
      echo "<br>";
      echo "<h3 class='text-center text-uppercase bg-warning text-warning'>Votre absence est enregistrée mais vous n'avez pas de justificatif, pensez à en ajouter un au bon format</h3>";
      echo "<a href='../templates/student.php' class='btn btn-default text-uppercase col-md-4 col-md-offset-4'>retour</a>";
      echo "<br>";
  }

  $insert = "INSERT INTO `abs`(`idstudent`, `absdate`, `morning`, `afternoon`, `justify`)
  VALUES ('$idstudent','$date','$morning','$afternoon','$name')";
  $addabs = mysqli_query($handle,$insert);
}

include("../templates/footer.php");


 ?>
