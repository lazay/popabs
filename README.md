Aurélie Cliquet, Ingrid Piton 2016
App absences gestion
Popschool promo Alan Turing

- You need to have a server running php and phpmyadmin

- Open your phpmyadmin, create a new empty database, import DB-sample.sql in it

- Edit config.sample.php in your text editor and modify the information to
 match your server and database :

 $handle = mysqli_connect("IP_server","phpmyadmin_username","phpmyadmin_password","database_name");

- The database contain an only example user as admin, you can now sign with admin as login and password

- The users you will add in the app next will use their last name as login
