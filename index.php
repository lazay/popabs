<?php
include("./templates/header.php");
?>
<div class='container'>
  <div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
      <div class="thumbnail log">
        <img src="templates/logo.jpg" alt="...">
        <div class="caption">
          <h3 class='text-center'><strong>Connexion</strong></h3>
          <form action="./core/login.php" method="get">
              <input class="btn-block text-center" name='username' type="text" placeholder="Username">
              <input class="btn-block text-center" name='password' type="password" placeholder="Password">
              <button class="btn btn-log btn-block text-center text-uppercase" type="submit">
              <strong>log in</strong></button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include("./templates/footer.php"); ?>
